import sqlite3
from argparse import ArgumentParser

# create sqlite table
create_joke_table_statement = """
    CREATE TABLE IF NOT EXISTS Jokes (
        joke_id INTEGER PRIMARY KEY AUTOINCREMENT,
        title TEXT,
        content TEXT,
        image_url TEXT,
        date TEXT,
        score REAL,
        nickname TEXT,
        user_id TEXT
    );
"""

# create sqlite table
create_joke_votes_table_statement = """
    CREATE TABLE IF NOT EXISTS JokeVotes (
        user_id TEXT,
        date TEXT,
        joke_id INTEGER,
        score_change REAL
    );
"""

# create sqlite table
create_joke_msgs_table_statement = """
    CREATE TABLE IF NOT EXISTS JokeMsgs (
        msg_id INTEGER PRIMARY KEY AUTOINCREMENT,
        msg_type TEXT,
        msg_text TEXT,
        joke_id INTEGER,
        user_id TEXT,
        timestamp TEXT
    );
"""

# create sqlite table
create_user_badges_table_statement = """
    CREATE TABLE IF NOT EXISTS UserBadges (
        user_id TEXT,
        type TEXT,
        date TEXT
    );
"""
create_warrior_image_table_statement = """
    CREATE TABLE IF NOT EXISTS WarriorImages (
        image_id INTEGER PRIMARY KEY AUTOINCREMENT,
        name TEXT,
        part_type TEXT,
        data_uri TEXT
    );
"""

create_user_warrior_image_table_statement = """
    CREATE TABLE IF NOT EXISTS UserWarriorImages (
        user_id TEXT,
        image_info_json TEXT
    );
"""

if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument("--db_path", type=str, help="sqlite db apth", default="example.db")
    args = parser.parse_args()

    con = sqlite3.connect(args.db_path)

    cur = con.cursor()

    # Create table
    cur.execute(create_joke_table_statement)
    cur.execute(create_joke_votes_table_statement)
    cur.execute(create_joke_msgs_table_statement)
    cur.execute(create_user_badges_table_statement)
    cur.execute(create_warrior_image_table_statement)
    cur.execute(create_user_warrior_image_table_statement)

    # Save (commit) the changes
    con.commit()

    # We can also close the connection if we are done with it.
    # Just be sure any changes have been committed or they will be lost.
    con.close()
