import logging
from dataclasses import dataclass

import streamlit as st

# apps
from config import SupportPeriodTime
from module.issue_selector import IssueSelector
from tools.sqlite_client import SqliteClient
from tools.tool import set_logger, display_image_or_video

set_logger()


@dataclass
class Champaign:
    name: str
    issue: str
    date_start: str
    date_end: str
    score: str
    top_joke_id: int


class ChampaignSelector:
    def __init__(self, db_client=None) -> None:
        self.issue_selector = IssueSelector()
        if db_client is None:
            self.db_client = SqliteClient()
        else:
            self.db_client = db_client

    def get_champaign(self, period_time: SupportPeriodTime):
        issue, date_start, date_end = self.issue_selector.get_issue(period_time)

        jokes = self.db_client.get_jokes(start_date=date_start)

        if not jokes:
            return Champaign(
                name="從缺",
                issue=issue,
                date_start=date_start,
                date_end=date_end,
                score=-1,
                top_joke_id=None,
            )

        top_joke = max(jokes, key=lambda x: x.score)
        return Champaign(
            name=top_joke.nickname,
            issue=issue,
            date_start=date_start,
            date_end=date_end,
            score=top_joke.score,
            top_joke_id=top_joke.joke_id,
        )


class HomePage:
    def __init__(self) -> None:
        self.db_client = SqliteClient()

    def ranking_template(self, period_time, day_issue, date_start):
        if period_time is SupportPeriodTime.DAY:
            return f"天下就可大會 {day_issue} 期 ({date_start})-日冠軍"

        if period_time is SupportPeriodTime.WEEK:
            return f"天下就可大會 {day_issue} 期 ({date_start})-週冠軍"

        if period_time is SupportPeriodTime.MONTH:
            return f"天下就可大會 {day_issue} 期 ({date_start})-月冠軍"

        raise NotImplementedError

    def get_page(self):
        st.title("今天，你準備好惹嗎")
        champaign_selector = ChampaignSelector(db_client=self.db_client)

        with st.container():
            st.subheader("排行榜")
            cols = st.columns(len(SupportPeriodTime))
            for period, col in zip(SupportPeriodTime, cols):
                with col:
                    logging.info(period)
                    champaign = champaign_selector.get_champaign(period)

                    logging.info(period)
                    ranking_text = self.ranking_template(
                        period_time=period,
                        day_issue=champaign.issue,
                        date_start=champaign.date_start,
                    )
                    with st.expander(ranking_text):
                        st.subheader(f"得獎者: {champaign.name}")
                        if champaign.top_joke_id is not None:
                            champaign_joke = self.db_client.get_joke_by_id(champaign.top_joke_id)
                            st.subheader(
                                f"得獎就可: [{champaign_joke.joke_id}] {champaign_joke.joke_title}"
                            )
                            st.text(champaign_joke.joke_content)
                            if champaign_joke.joke_image_url:
                                display_image_or_video(champaign_joke.joke_image_url)


st.set_page_config(layout="wide")
home_page = HomePage()
home_page.get_page()
