import arrow
import os
import logging
from config import SupportPeriodTime


class IssueSelector:
    def __init__(self) -> None:
        self.system_start_time = arrow.get("2022-05-23")
        self.timezone = os.environ.get("TIMEZONE", "GMT")

    def get_issue(self, period_time: SupportPeriodTime, period_shift: int = 0):
        now = arrow.now(self.timezone)
        div_period = (now - self.system_start_time).days // period_time.value
        div_period += period_shift
        date_start = self.system_start_time.shift(days=div_period * period_time.value)
        date_start_str = date_start.format("YYYY-MM-DD")
        date_end = date_start.shift(days=period_time.value)
        date_end_str = date_end.format("YYYY-MM-DD")
        return self.issue_format(div_period), date_start_str, date_end_str

    def issue_format(self, issue: int):
        issue_str = "0000" + str(issue)
        logging.info(issue_str)
        return issue_str[-4:]
