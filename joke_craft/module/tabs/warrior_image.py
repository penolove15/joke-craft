import base64
import copy
import io
import json
import streamlit as st
import numpy as np
import os
import random
import re
import requests
import time

from dataclasses import dataclass
from enum import Enum
from PIL import Image
from urllib import request

# apps
from tools.google_auth_client import GoogleAuthClient
from tools.sqlite_client import (
    UserWarriorImagesSqliteClient,
    WarriorImagesSqliteClient,
    WarriorImageModel,
)


class MaterialFields(Enum):
    DISPLAT_NAME = "dispaly_name"
    INFO = "info"
    FROM = "from"


class WarriorBodyParts(Enum):
    LEFT_FOOT = "left_foot"
    LEFT_HAND = "left_hand"
    HEAD = "head"
    RIGHT_HAND = "right_hand"
    RIGHT_FOOT = "right_foot"


@dataclass
class WarriorImageTableManager:
    db_client = WarriorImagesSqliteClient()

    def get_materials(self, body_part: WarriorBodyParts):
        image_models = self.db_client.get_images_by_part_type(body_part.value)
        materials_name = [image_model.name for image_model in image_models]
        infos = image_models
        return materials_name, infos

    def get_image_data(self, material_dict):
        image_model: WarriorImageModel = material_dict[MaterialFields.INFO.value]
        with request.urlopen(image_model.data_uri) as response:
            image = Image.open(io.BytesIO(response.read()))
            return np.asarray(image)

    def _request_image_and_endcode(self, url):
        response = requests.get(url)
        image = Image.open(io.BytesIO(response.content)).resize((300, 300))
        random_suffix = str(random.random()).split(".")[1]
        image_path = f"./joke-craft/joke_craft/image/tmp_{random_suffix}.jpg"
        image.save(image_path)
        encoded_image = base64.b64encode(open(image_path, "rb").read()).decode("ascii")
        os.remove(image_path)

        return f"data:image/jpg;base64,{encoded_image}"

    def insert_image(self, image_name, select_part, image_url):

        if re.findall("^http.*", image_url):
            image_url = self._request_image_and_endcode(image_url)

        if not re.findall("^data.*", image_url):
            return False

        self.db_client.insert_image(image_name, select_part, data_uri=image_url)


@dataclass
class WarriorImageLocalManager:
    image_root_folder: str = "./joke-craft/joke_craft/image"

    @property
    def left_foot(self):
        return (
            f"{self.image_root_folder}/"
            f"{WarriorBodyParts.LEFT_FOOT.value}/"
            f"{self.left_foot_type}.png"
        )  # noqa E501

    @property
    def left_hand(self):
        return (
            f"{self.image_root_folder}/"
            f"{WarriorBodyParts.LEFT_HAND.value}/"
            f"{self.left_hand_type}.png"
        )  # noqa E501

    @property
    def head(self):
        return (
            f"{self.image_root_folder}/" f"{WarriorBodyParts.HEAD.value}/" f"{self.head_type}.png"
        )  # noqa E501

    @property
    def right_hand(self):
        return (
            f"{self.image_root_folder}/"
            f"{WarriorBodyParts.RIGHT_HAND.value}/"
            f"{self.right_hand_type}.png"
        )  # noqa E501

    @property
    def right_foot(self):
        return (
            f"{self.image_root_folder}/"
            f"{WarriorBodyParts.RIGHT_FOOT.value}/"
            f"{self.right_foot_type}.png"
        )  # noqa E501

    def get_materials(self, body_part: WarriorBodyParts):
        materials_name = [
            file_name.split(".")[0]
            for file_name in os.listdir(f"{self.image_root_folder}/{body_part.value}")
        ]
        infos = [None for i in range(len(materials_name))]
        return materials_name, infos

    def get_image_data(self, part, material_dict):
        material = material_dict[MaterialFields.DISPLAT_NAME.value]
        setattr(self, f"{part}_type", material)
        return np.asarray(Image.open(getattr(self, part)))


@dataclass
class WarriorImageSourceManager:
    local_image_manager = WarriorImageLocalManager()
    table_image_manager = WarriorImageTableManager()
    user_image_db_client = UserWarriorImagesSqliteClient()
    LOCAL = "local"
    TABLE = "table"

    def support_materials(self, body_part: WarriorBodyParts):

        support_source_get_funcs = [
            (self.LOCAL, self.local_image_manager.get_materials),
            (self.TABLE, self.table_image_manager.get_materials),
        ]

        return_materials = []
        for from_source, func in support_source_get_funcs:
            materials, infos = func(body_part)
            for materials_name, info in zip(materials, infos):
                return_materials.append(
                    {
                        MaterialFields.DISPLAT_NAME.value: materials_name,
                        MaterialFields.INFO.value: info,
                        MaterialFields.FROM.value: from_source,
                    }
                )
        return return_materials

    def get_images_data(self, selected_materials):
        images_data = {}
        for part, material_dict in selected_materials.items():
            from_source = material_dict[MaterialFields.FROM.value]
            if from_source == self.LOCAL:
                images_data[f"{part}_data"] = self.local_image_manager.get_image_data(
                    part, material_dict
                )  # noqa E501
            if from_source == self.TABLE:
                images_data[f"{part}_data"] = self.table_image_manager.get_image_data(
                    material_dict
                )  # noqa E501
        return images_data

    def get_preset_index(self, body_part: WarriorBodyParts, user_image_info, support_matertials):

        DISPLAT_NAME = MaterialFields.DISPLAT_NAME.value
        FROM = MaterialFields.FROM.value
        INFO = MaterialFields.INFO.value

        target_part = user_image_info[body_part.value]
        index = 0
        for i in range(len(support_matertials)):
            current_material = support_matertials[i]
            if current_material[FROM] != target_part[FROM]:  # noqa E501
                continue

            if current_material[FROM] == self.LOCAL:
                if current_material[DISPLAT_NAME] != target_part[DISPLAT_NAME]:  # noqa E501
                    continue

            if current_material[FROM] == self.TABLE:
                image_model: WarriorImageModel = current_material[INFO]

                if image_model.image_id != target_part[INFO]["image_id"]:
                    continue
            index = i
            break
        return index

    def select_body_materials_to_json(self, select_body_materials):
        select_body_materials = copy.deepcopy(select_body_materials)
        FROM = MaterialFields.FROM.value
        INFO = MaterialFields.INFO.value
        for part, material in select_body_materials.items():
            if material[FROM] == self.TABLE:
                image_model: WarriorImageModel = material[INFO]
                material[INFO] = {"image_id": image_model.image_id}
        return json.dumps(select_body_materials, ensure_ascii=False)

    def save_user_image_info(self, user_id, select_body_materials):
        image_info_json_str = self.select_body_materials_to_json(select_body_materials).strip()

        if not self.user_image_db_client.get_user_image_info(user_id):
            self.user_image_db_client.insert_user_image_info(user_id, image_info_json_str)
        else:
            self.user_image_db_client.update_user_image_info(user_id, image_info_json_str)

    def get_user_image_info(self, user_id):
        return self.user_image_db_client.get_user_image_info(user_id)


@dataclass
class WarriorImageGenerator:
    left_foot_data: np.ndarray
    left_hand_data: np.ndarray
    head_data: np.ndarray
    right_hand_data: np.ndarray
    right_foot_data: np.ndarray

    def load_image_data(self, array):
        return Image.fromarray(array).resize((300, 300))

    def generate(self):
        human_img = Image.open("./joke-craft/joke_craft/image/human.jpg")
        result_image = Image.new("RGB", (900, 1200), (255, 255, 255))
        result_image.paste(human_img, (300, 350))
        result_image.paste(self.load_image_data(self.left_hand_data), (0, 250))
        result_image.paste(self.load_image_data(self.right_hand_data), (600, 250))
        result_image.paste(self.load_image_data(self.left_foot_data), (35, 850))
        result_image.paste(self.load_image_data(self.right_foot_data), (565, 850))
        result_image.paste(self.load_image_data(self.head_data), (300, 50))
        return result_image


class WarriorImagePage:
    tab_name = "warrior_image"

    def __init__(self, google_auth_client: GoogleAuthClient) -> None:
        self.google_auth_client = google_auth_client
        self.google_auth_client.share_check_property(self)

    def get_warrior_image_page(self):
        st.header("Warrior Image")
        select_body_materials = {}
        warrior_image_source_manager = WarriorImageSourceManager()
        with st.container() as side_bar:  # noqa F841
            user_image_info = None
            if not self.is_anonymous_user:
                user_image_info = warrior_image_source_manager.get_user_image_info(
                    self.user_info_manager.get_user_id()
                )
            for col, body_part in zip(
                st.columns(len(WarriorBodyParts)),
                WarriorBodyParts,
            ):
                support_matertials = warrior_image_source_manager.support_materials(
                    body_part=body_part
                )
                with col:
                    select_index = st.selectbox(
                        body_part.value,
                        range(len(support_matertials)),
                        index=0
                        if not user_image_info
                        else (
                            warrior_image_source_manager.get_preset_index(
                                body_part,
                                user_image_info,
                                support_matertials,
                            )
                        ),
                        format_func=lambda index: support_matertials[index][
                            MaterialFields.DISPLAT_NAME.value
                        ],
                    )
                select_body_materials[body_part.value] = support_matertials[select_index]

        with st.container() as warrior_image_container:  # noqa F841
            if self.is_anonymous_user:
                st.write("登入才看得到你上次的設定喔ＯＡＯ！...(去 user profile")
            else:
                user_id = self.user_info_manager.get_user_id().split("@")[0]
                st.write(f"哈囉 {user_id} 準備好變強惹嗎？")
            images_data = warrior_image_source_manager.get_images_data(select_body_materials)
            warrior_image_generator = WarriorImageGenerator(**images_data)

            image_cols = st.columns(3)
            with image_cols[1]:
                st.image(np.asarray(warrior_image_generator.generate()))

            save_button_cols = st.columns(5)
            if not self.is_anonymous_user:
                with save_button_cols[-1]:
                    is_save_imag_submit = st.button("save")
                with save_button_cols[0]:
                    if is_save_imag_submit:
                        warrior_image_source_manager.save_user_image_info(
                            self.user_info_manager.get_user_id(), select_body_materials
                        )
                        st.write("儲存成功><!")

        with st.container() as image_upload:  # noqa F841
            st.header("Upload Image")
            if not self.is_anonymous_user:
                with st.form(key="others_msg_form"):
                    col_1, col_2 = st.columns(2)
                    with col_1:
                        image_name = st.text_input("照片名字要叫什麼咧")
                    with col_2:
                        select_part = st.selectbox(
                            "要放在哪個部位咧", [part.value for part in WarriorBodyParts]
                        )
                    image_url = st.text_input("image_url")
                    is_image_upload_submit = st.form_submit_button(label="Submit")

                    if is_image_upload_submit:
                        warrior_image_source_manager.table_image_manager.insert_image(
                            image_name, select_part, image_url
                        )
                        st.write("上傳成功><!")
                        st.write("即將重整.....")
                        time.sleep(5)
                        st.experimental_rerun()
            else:
                st.write(
                    "我們支持上傳照片功能歐，為維持風俗民情，登入才能放東西喔>.<! (去 user profile",
                )
