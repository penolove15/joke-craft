import asyncio

import streamlit as st
import pandas as pd
import plotly.express as px

from tools.google_auth_client import GoogleAuthClient
from tools.sqlite_client import SqliteClient


class LoginPage:
    tab_name = "Login"

    def __init__(self, google_auth_client: GoogleAuthClient) -> None:
        self.google_auth_client = google_auth_client

    @property
    def is_oath_env_varialbes_set_properly(self):
        return self.google_auth_client.is_oath_env_varialbes_set_properly

    def show_user_info(self, user_info):
        user_id = user_info["email"]
        user_name = user_id.split("@")[0]
        st.title(f"Hi {user_name}")
        with st.container():
            cols = st.columns(3)
            with cols[0]:
                st.image(user_info["picture"], use_column_width="always")

        with st.container():
            cols = st.columns([1, 1, 4])
            with cols[0]:
                st.button(
                    "Logout",
                    on_click=self.google_auth_client.user_info_manager.delete_user_info,
                )
            with cols[1]:
                st.button(
                    "Remember me (set cookie)",
                    on_click=self.google_auth_client.user_info_manager.set_cookie_user_info_from_session,  # noqa E501
                )

    def get_login_page(self):
        try:
            self.google_auth_client.set_google_auth_client()
            user_info = self.google_auth_client.get_user_info()
            if not user_info:
                raise Exception("failure to get user_info")
            self.show_user_info(user_info)

            user_id = user_info["email"]
            db_client = SqliteClient()
            joke_votes = db_client.get_votes_by_user_id(user_id)

            # joke preference
            df = pd.DataFrame(joke_votes, columns=["user_id", "date", "joke_id", "score_change"])
            st.write(f"vote records: {df.shape[0]}")
            score_res = (
                df.groupby(["user_id", "joke_id"])["score_change"]
                .agg(["sum", "count"])
                .reset_index()
            )
            score_res.columns = ["user_id", "joke_id", "vote_sum", "vote_count"]
            st.dataframe(score_res)

            # date distribution
            st.write("vote activities:")
            date_dist = df.groupby("date")["date"].agg(["count"]).reset_index()
            date_dist["date"] = pd.to_datetime(date_dist["date"])

            fig = px.line(date_dist, x="date", y="count", title="評分次數", markers=True)
            st.plotly_chart(fig, use_container_width=True)

        except Exception:
            asyncio.run(self.google_auth_client.get_login_page())
