import streamlit as st

from module.tabs.badges import ShowBadgesPage
from module.tabs.warrior_image import WarriorImagePage
from module.tabs.login import LoginPage
from tools.google_auth_client import GoogleAuthClient


class ProfilePageWithTabs:
    def __init__(self) -> None:
        self.google_auth_client = GoogleAuthClient()
        self.login_page = LoginPage(self.google_auth_client)
        self.warrior_image_page = WarriorImagePage(self.google_auth_client)
        self.show_badges_page = ShowBadgesPage(self.google_auth_client)

        self.tab_names = [
            self.login_page.tab_name,
            self.show_badges_page.tab_name,
            self.warrior_image_page.tab_name,
        ]
        self.tab_content_mapping = {
            self.warrior_image_page.tab_name: self.warrior_image_page.get_warrior_image_page,
            self.show_badges_page.tab_name: self.show_badges_page.get_badge_page,
            self.login_page.tab_name: self.login_page.get_login_page,
        }

    def get_page_with_tabs(self):
        if not self.login_page.is_oath_env_varialbes_set_properly:
            st.write("oath Env variables not set properly")
            return

        if "tabs" not in st.session_state:
            st.session_state["tabs"] = self.tab_names
        tabs = st.tabs(st.session_state["tabs"])
        for tab_name in self.tab_names:
            with tabs[self.tab_names.index(tab_name)]:
                self.tab_content_mapping[tab_name]()

    def get_page(self):
        raise NotImplementedError

    def register_page_into_tab(self, tab_name, to_first=False):
        tab_index = len(self.tab_names)
        if to_first:
            tab_index = 0

        self.tab_names.insert(tab_index, self.tab_name)
        self.tab_content_mapping.update({tab_name: self.get_page})
