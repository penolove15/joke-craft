from collections import Counter

import streamlit as st
import arrow

from config import ANONYMOUS_USER, Badges
from tools.sqlite_client import SqliteClient
from tools.google_auth_client import GoogleAuthClient


BADGE_IMAGE_MAPPING = {
    Badges.MARK_TWAIN: "./joke-craft/joke_craft/image/badges/mark_twain.jpg",
    Badges.PERFECT_ATTENDANCE_DAILY_CHAMPION: "./joke-craft/joke_craft/image/badges/perfect_attendance.png",  # noqa
    Badges.PERFECT_ATTENDANCE_JOKE_WARRIOR: "./joke-craft/joke_craft/image/badges/perfect_attendance.png",  # noqa
    Badges.PERFECT_ATTENDANCE_JOKE_VOTER: "./joke-craft/joke_craft/image/badges/perfect_attendance.png",  # noqa
    Badges.SENIOR_JOKE_VOTER: "./joke-craft/joke_craft/image/badges/senior_voter.png",
    Badges.ICE_KING: "./joke-craft/joke_craft/image/badges/ice_king.jpg",
    Badges.SEVEN_NIGHT_KING: "./joke-craft/joke_craft/image/badges/seven-night-king.png",
    Badges.DORA_KING: "./joke-craft/joke_craft/image/badges/dora-king.png",
    Badges.THREE_SIX_FIVE_KING: "./joke-craft/joke_craft/image/badges/365king.png",
}

BADGE_REASON_MAPPING = {
    Badges.MARK_TWAIN: "日週月冠軍-幽默大師: 幽默了",
    Badges.PERFECT_ATTENDANCE_DAILY_CHAMPION: "全勤日冠軍: 週一到週五都拿過冠軍",
    Badges.PERFECT_ATTENDANCE_JOKE_WARRIOR: "全勤笑話鬥士: 週一到週五都投過搞",
    Badges.PERFECT_ATTENDANCE_JOKE_VOTER: "全勤鄉民: 週一到週五都投過票",
    Badges.SENIOR_JOKE_VOTER: "資深鄉民: 投票破百",
    Badges.ICE_KING: "大將青雉: 笑話分數突破下限",
    Badges.SEVEN_NIGHT_KING: "七夕之王",
    Badges.DORA_KING: "多啦王",
    Badges.THREE_SIX_FIVE_KING: "365王",
}


class ShowBadgesPage:
    tab_name = "Badges"

    def __init__(self, google_auth_client: GoogleAuthClient) -> None:
        self.google_auth_client = google_auth_client
        self.user_info = google_auth_client.user_info_manager
        self.session_user_id_ = self.google_auth_client.session_user_id_

    @property
    def session_user_id(self):
        if self.session_user_id_ is not None:
            return self.session_user_id_
        else:
            return ANONYMOUS_USER

    def plot_joke_champion_badge(self, column, type, count):
        with column:
            st.text(f"{type}: {count} 次")
            if count == 0:
                st.image("./joke-craft/joke_craft/image/badges/badge.png")
            elif count >= 7:
                st.image("./joke-craft/joke_craft/image/badges/gold.png")
            elif count >= 4:
                st.image("./joke-craft/joke_craft/image/badges/silver.png")
            elif count >= 1:
                st.image("./joke-craft/joke_craft/image/badges/bronze.png")

    def hidden_badge_check(self, column, is_reach, title, image, reason=""):
        with column:
            if is_reach:
                st.text(title)
                if reason:
                    st.text(reason)
                st.image(image)
            else:
                st.text("?????????")
                st.image("./joke-craft/joke_craft/image/badges/badge.png")

    def show_joke_related_badges(self, db_client):
        user_badges = db_client.get_user_badges_by_user_id(self.session_user_id)
        st.subheader("就可成就")
        badge_counter = Counter(i.type for i in user_badges)
        # get numbers of daily champion from users
        col1, col2, col3, col4, col5, col6, col7 = st.columns(7)
        self.plot_joke_champion_badge(
            column=col1,
            type=Badges.DAILY_CHAMPION.value,
            count=badge_counter[Badges.DAILY_CHAMPION.name],
        )

        self.plot_joke_champion_badge(
            column=col2,
            type=Badges.WEEKLY_CHAMPION.value,
            count=badge_counter[Badges.WEEKLY_CHAMPION.name],
        )

        self.plot_joke_champion_badge(
            column=col3,
            type=Badges.MONTHLY_CHAMPION.value,
            count=badge_counter[Badges.MONTHLY_CHAMPION.name],
        )

        # hidden badges - perfect attendance as daily champion
        week_days = set(
            arrow.get(i.date).weekday()
            for i in user_badges
            if i.type == Badges.DAILY_CHAMPION.name
        )
        is_reach = len(week_days) >= 5
        self.hidden_badge_check(
            column=col4,
            is_reach=is_reach,
            reason=BADGE_REASON_MAPPING[Badges.PERFECT_ATTENDANCE_DAILY_CHAMPION],
            title=Badges.PERFECT_ATTENDANCE_DAILY_CHAMPION.value,
            image=BADGE_IMAGE_MAPPING[Badges.PERFECT_ATTENDANCE_DAILY_CHAMPION],
        )

        # hidden badges - mark-twain
        is_reach = all(
            badge_counter[i] > 0
            for i in [
                Badges.DAILY_CHAMPION.name,
                Badges.WEEKLY_CHAMPION.name,
                Badges.MONTHLY_CHAMPION.name,
            ]
        )
        self.hidden_badge_check(
            column=col5,
            is_reach=is_reach,
            title=Badges.MARK_TWAIN.value,
            reason=BADGE_REASON_MAPPING[Badges.MARK_TWAIN],
            image=BADGE_IMAGE_MAPPING[Badges.MARK_TWAIN],
        )

        # hidden badge - perfect attendance as joke warrior
        user_jokes = db_client.get_joke_by_user_id(self.session_user_id)
        week_days = set(arrow.get(i.date).weekday() for i in user_jokes)
        is_reach = len(week_days) >= 5
        self.hidden_badge_check(
            column=col6,
            is_reach=is_reach,
            title=Badges.PERFECT_ATTENDANCE_JOKE_WARRIOR.value,
            reason=BADGE_REASON_MAPPING[Badges.PERFECT_ATTENDANCE_JOKE_WARRIOR],
            image=BADGE_IMAGE_MAPPING[Badges.PERFECT_ATTENDANCE_JOKE_WARRIOR],
        )

        # hidden badge - ice king
        is_reach = user_jokes and (min(i.score for i in user_jokes) <= 80)
        self.hidden_badge_check(
            column=col7,
            is_reach=is_reach,
            title=Badges.ICE_KING.value,
            reason=BADGE_REASON_MAPPING[Badges.ICE_KING],
            image=BADGE_IMAGE_MAPPING[Badges.ICE_KING],
        )

    def show_vote_related_badges(self, db_client):
        joke_votes = db_client.get_votes_by_user_id(self.session_user_id)

        st.subheader("鄉民成就")
        # get numbers of daily champion from users
        col1, col2, *_ = st.columns(6)

        is_reach = len(joke_votes) >= 100
        self.hidden_badge_check(
            column=col1,
            is_reach=is_reach,
            title=Badges.SENIOR_JOKE_VOTER.value,
            reason=BADGE_REASON_MAPPING[Badges.SENIOR_JOKE_VOTER],
            image=BADGE_IMAGE_MAPPING[Badges.SENIOR_JOKE_VOTER],
        )

        week_days = set(arrow.get(i.date).weekday() for i in joke_votes)
        is_reach = len(week_days) >= 5
        self.hidden_badge_check(
            column=col2,
            is_reach=is_reach,
            reason=BADGE_REASON_MAPPING[Badges.PERFECT_ATTENDANCE_JOKE_VOTER],
            title=Badges.PERFECT_ATTENDANCE_JOKE_VOTER.value,
            image=BADGE_IMAGE_MAPPING[Badges.PERFECT_ATTENDANCE_JOKE_VOTER],
        )

    def show_special_badges(self, db_client):
        user_badges = db_client.get_user_badges_by_user_id(self.session_user_id)
        badge_counter = Counter(i.type for i in user_badges)

        st.subheader("特殊成就")

        col1, col_dora, col_365, *_ = st.columns(6)
        is_reach = badge_counter[Badges.SEVEN_NIGHT_KING.name] > 0
        self.hidden_badge_check(
            column=col1,
            is_reach=is_reach,
            reason=BADGE_REASON_MAPPING[Badges.SEVEN_NIGHT_KING],
            title=Badges.SEVEN_NIGHT_KING.value,
            image=BADGE_IMAGE_MAPPING[Badges.SEVEN_NIGHT_KING],
        )

        is_reach = badge_counter[Badges.DORA_KING.name] > 0
        self.hidden_badge_check(
            column=col_dora,
            is_reach=is_reach,
            reason=BADGE_REASON_MAPPING[Badges.DORA_KING],
            title=Badges.DORA_KING.value,
            image=BADGE_IMAGE_MAPPING[Badges.DORA_KING],
        )

        is_reach = badge_counter[Badges.THREE_SIX_FIVE_KING.name] > 0
        self.hidden_badge_check(
            column=col_365,
            is_reach=is_reach,
            reason=BADGE_REASON_MAPPING[Badges.THREE_SIX_FIVE_KING],
            title=Badges.THREE_SIX_FIVE_KING.value,
            image=BADGE_IMAGE_MAPPING[Badges.THREE_SIX_FIVE_KING],
        )

    def get_badge_page(self):
        st.title("天下第一就可大會 你的徽章收藏")
        if self.session_user_id == ANONYMOUS_USER:
            st.write("嗨, 陌生人, 陌生人不會有徽章的喇！！快去 login!")
            return
        else:
            st.write(f"嗨, {self.session_user_id}, 以下是你的徽章")
            db_client = SqliteClient()
            self.show_joke_related_badges(db_client=db_client)

            self.show_vote_related_badges(db_client=db_client)

            self.show_special_badges(db_client=db_client)
