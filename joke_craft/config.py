from enum import Enum


class SupportPeriodTime(Enum):
    DAY = 1
    WEEK = 7
    MONTH = 28


class JokeMsgType(Enum):
    AUTHOR = "author"
    OTHERS = "others"


USER_INFO = "USER_INFO"

ANONYMOUS_USER = "ANONYMOUS"


# badges
class Badges(Enum):
    DAILY_CHAMPION = "日笑話冠軍"
    WEEKLY_CHAMPION = "週笑話冠軍"
    MONTHLY_CHAMPION = "月笑話冠軍"
    MARK_TWAIN = "幽默大師-馬克吐溫"
    ICE_KING = "大將青雉"
    PERFECT_ATTENDANCE_DAILY_CHAMPION = "全勤日冠軍"
    PERFECT_ATTENDANCE_JOKE_WARRIOR = "全勤笑話鬥士"
    PERFECT_ATTENDANCE_JOKE_VOTER = "全勤鄉民"
    SENIOR_JOKE_VOTER = "資深鄉民"
    SEVEN_NIGHT_KING = "七夕之王"
    DORA_KING = "多啦王"
    THREE_SIX_FIVE_KING = "365王"
