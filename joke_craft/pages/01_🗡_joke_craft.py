import os
import random
from functools import partial

import arrow
import streamlit as st

# apps
from config import ANONYMOUS_USER, SupportPeriodTime
from module.issue_selector import IssueSelector
from tools.sqlite_client import SqliteClient
from tools.user_info_manager import UserInfoManager
from tools.tool import display_image_or_video


class JokeCraftPage:
    def __init__(self) -> None:
        self.issue_selector = IssueSelector()
        self.db_client = SqliteClient()
        self.user_info = UserInfoManager()
        self.session_user_id_ = self.user_info.get_user_id()

    @property
    def session_user_id(self):
        if self.session_user_id_ is not None:
            return self.session_user_id_
        else:
            return ANONYMOUS_USER

    def get_page(self):
        st.title("天下第一就可大會 JOKE CRAFT")
        if self.session_user_id == ANONYMOUS_USER:
            st.write("嗨, 陌生人, 不想當陌生人? 快去 user_profile 洗刷刷, 登入才會有成就!!")
        else:
            st.write(f"嗨, {self.session_user_id}, 準備好守住嘴角了嗎?")

        period = st.sidebar.radio(
            "suggest craft period:",
            (
                SupportPeriodTime.DAY.name,
                SupportPeriodTime.WEEK.name,
                SupportPeriodTime.MONTH.name,
            ),
            index=1,
        )
        timezone = os.environ.get("TIMEZONE", "GMT")
        hour_shift = int(os.environ.get("CRAFT_HOUR_SHIFT", 0))
        now = (
            arrow.now(timezone)
            .shift(hours=hour_shift)
            .shift(days=-(SupportPeriodTime[period].value - 1))
        )

        date_start = now.datetime
        date_start = str(st.sidebar.date_input("craft period starts from:", date_start))

        jokes = self.db_client.get_jokes(date_start)

        if len(jokes) < 2:
            st.subheader("投稿人數不足，現在投稿，你就是第一惹喔！！！")
            return

        n_compares = int(
            st.sidebar.number_input("candidates", min_value=2, max_value=len(jokes), value=2)
        )

        selected_jokes = random.sample(jokes, n_compares)
        cols = st.columns(n_compares)

        candidates = set(i.joke_id for i in selected_jokes)
        for joke, col in zip(selected_jokes, cols):
            with col:

                st.subheader(joke.joke_title)

                with st.expander(f"[{joke.joke_id}] 你可能會很意外的 point", expanded=False):
                    st.text(joke.joke_content)
                    if joke.joke_image_url:
                        display_image_or_video(joke.joke_image_url)

                    partial_winner_update = partial(
                        self.db_client.winner_joke_update,
                        candidates=candidates,
                        winner=joke.joke_id,  # noqa E501
                        vote_user_id=self.session_user_id,
                    )
                    st.button("選我 選我", key=joke.joke_id, on_click=partial_winner_update)


st.set_page_config(layout="wide")
joke_craft_page = JokeCraftPage()
joke_craft_page.get_page()
