from collections import defaultdict
from dataclasses import dataclass
from typing import List

import numpy as np
import pandas as pd
import plotly.express as px
import streamlit as st

# apps
from tools.sqlite_client import JokeModel, SqliteClient
from tools.tool import get_now

JOKE_AMOUNT = "投稿數"
JOKE_POWER = "笑力"
JOKE_SCORE_MEDIAN = "笑力中位數"


@dataclass
class WarriorStat:
    nickname: str
    joke_amount: int
    joke_power: int
    joke_score_median: int
    jokes: List[JokeModel]


@st.cache(ttl=0)
def get_all_warrior_stat(start):
    db_client = SqliteClient()
    jokes = db_client.get_jokes(start_date=start)

    user_jokes = defaultdict(list)
    for joke in jokes:
        user_jokes[joke.nickname].append(joke)

    user_joke_count = []
    user_joke_powers = []
    user_joke_score_medians = []
    user_joke_stat = {}
    for nickname, jokes in user_jokes.items():
        joke_scores = [i.score for i in jokes]
        joke_power = sum(joke_scores)
        joke_score_median = np.median(joke_scores)
        user_joke_stat[nickname] = WarriorStat(
            nickname=nickname,
            joke_amount=len(jokes),
            joke_power=joke_power,
            joke_score_median=joke_score_median,
            jokes=jokes,
        )

        user_joke_count.append(len(jokes))
        user_joke_powers.append(joke_power)
        user_joke_score_medians.append(joke_score_median)

    return (
        user_joke_stat,
        np.mean(user_joke_count),
        np.mean(user_joke_powers),
        np.mean(user_joke_score_medians),
    )


class WarriorStatPage:
    def plot_metric(
        self,
        metric_container,
        metric_name,
        metric_value,
        uplift_value,
    ):
        metric_container.metric(metric_name, metric_value, f"{uplift_value}%")

    def metric_data_template(self, metric_name, metric_value, uplift_value):
        return {
            "metric_name": metric_name,
            "metric_value": metric_value,
            "uplift_value": uplift_value,
        }

    def submission_metric_data(self, author_stat, organic_mean_joke_count):
        uplift_submission = int(
            (author_stat.joke_amount - organic_mean_joke_count) / organic_mean_joke_count * 100
        )

        return self.metric_data_template(JOKE_AMOUNT, author_stat.joke_amount, uplift_submission)

    def power_metric_data(self, author_stat, organic_mean_joke_power):
        uplift_power = int(
            (author_stat.joke_power - organic_mean_joke_power) / organic_mean_joke_power * 100
        )

        return self.metric_data_template(JOKE_POWER, author_stat.joke_power, uplift_power)

    def power_median_metric_data(self, author_stat, organic_mean_joke_score_median):
        uplift_score_median = int(
            (author_stat.joke_score_median - organic_mean_joke_score_median)
            / organic_mean_joke_score_median
            * 100
        )

        return self.metric_data_template(
            JOKE_SCORE_MEDIAN, author_stat.joke_score_median, uplift_score_median
        )  # noqa E501

    def get_author_list(self, user_joke_stats, order):
        if order == JOKE_AMOUNT:
            author_orders = [
                i[0] for i in sorted(user_joke_stats.items(), key=lambda x: -x[1].joke_amount)
            ]
        elif order == JOKE_POWER:
            author_orders = [
                i[0] for i in sorted(user_joke_stats.items(), key=lambda x: -x[1].joke_power)
            ]
        elif order == JOKE_SCORE_MEDIAN:
            author_orders = [
                i[0]
                for i in sorted(user_joke_stats.items(), key=lambda x: -x[1].joke_score_median)
            ]
        else:
            raise Exception("not supported order")

        return author_orders

    def get_page(self):
        now = get_now()
        start = now.shift(days=-29).datetime
        start = str(st.sidebar.date_input("warrior stat starts from:", start))
        (
            user_joke_stats,
            organic_mean_joke_count,
            organic_mean_joke_power,
            organic_mean_joke_score_median,
        ) = get_all_warrior_stat(start)

        order = st.sidebar.selectbox(
            "Sort-by", [JOKE_AMOUNT, JOKE_POWER, JOKE_SCORE_MEDIAN], index=1
        )

        author_orders = self.get_author_list(user_joke_stats=user_joke_stats, order=order)

        author = st.sidebar.selectbox("Which Warrior You Want to Know?", author_orders)

        # metric
        author_stat = user_joke_stats[author]
        st.subheader(f"鬥士: {author}")

        col1, col2, col3 = st.columns(3)

        st.text("metric compared with other warriors:")

        self.plot_metric(
            metric_container=col1,
            **self.submission_metric_data(author_stat, organic_mean_joke_count),
        )
        self.plot_metric(
            metric_container=col2, **self.power_metric_data(author_stat, organic_mean_joke_power)
        )
        self.plot_metric(
            metric_container=col3,
            **self.power_median_metric_data(author_stat, organic_mean_joke_score_median),
        )

        # line plot
        df = pd.DataFrame(
            [(joke.date, joke.score) for joke in author_stat.jokes], columns=["date", "score"]
        )
        df = df.groupby("date")["score"].sum().reset_index()
        df["date"] = pd.to_datetime(df["date"])

        fig = px.line(df, x="date", y="score", title="笑力分佈", markers=True)
        st.plotly_chart(fig, use_container_width=True)


st.set_page_config(layout="wide")
warrior_stat_page = WarriorStatPage()
warrior_stat_page.get_page()
