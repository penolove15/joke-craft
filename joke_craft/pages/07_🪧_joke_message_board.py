import math
from typing import Optional

import arrow
import streamlit as st

from config import ANONYMOUS_USER, JokeMsgType, SupportPeriodTime
from module.issue_selector import IssueSelector
from tools.sqlite_client import JokeModel, SqliteClient
from tools.user_info_manager import UserInfoManager

MSG_PER_PAGE = 10


class JokeMessageBoardPage:
    def __init__(self) -> None:
        self.issue_selector = IssueSelector()
        self.db_client = SqliteClient()
        self.user_info = UserInfoManager()
        self.session_user_id_ = self.user_info.get_user_id()

    @property
    def session_user_id(self):
        if self.session_user_id_ is not None:
            return self.session_user_id_
        else:
            return ANONYMOUS_USER

    def get_joke_id(self) -> Optional[int]:
        period = st.sidebar.radio(
            "craft period:",
            (
                SupportPeriodTime.DAY.name,
                SupportPeriodTime.WEEK.name,
                SupportPeriodTime.MONTH.name,
            ),
            index=1,
        )
        _, date_start, date_end = self.issue_selector.get_issue(SupportPeriodTime[period])

        date_start = arrow.get(date_start).datetime
        date_start = str(st.sidebar.date_input("craft period starts from:", date_start))

        date_end = arrow.get(date_end).datetime
        date_end = str(st.sidebar.date_input("craft period ends at:", date_end))

        jokes = self.db_client.get_jokes(date_start, date_end)
        if len(jokes) == 0:
            return None

        jokes = sorted(jokes, key=lambda joke: joke.joke_id)
        joke_reprs = [f"{joke.joke_id} - {joke.nickname} - {joke.joke_title}" for joke in jokes]

        selected_joke_repr: str = st.sidebar.selectbox("joke:", joke_reprs)
        selected_joke_id = int(selected_joke_repr.split(" - ")[0])
        return selected_joke_id

    def show_joke(self, joke: JokeModel):
        st.subheader(joke.joke_title)
        st.text(f"commit_date: {joke.date}, author: {joke.nickname}\n" f"score: {joke.score}\n")
        with st.expander(f"[{joke.joke_id}] 你可能會很意外的 point", expanded=True):
            st.text(joke.joke_content)
            if joke.joke_image_url:
                try:
                    st.image(joke.joke_image_url)
                except Exception:
                    st.text("J個圖片網址怪怪的, 請靜待修改上線, 或者聯絡on-duty")

    def get_page(self):
        st.title("天下第一就可大會 MESSAGE BOARD")

        joke_id = self.get_joke_id()
        if joke_id is None:
            st.subheader("這段期間沒有投稿唷 ^.,>")
            return

        joke = self.db_client.get_joke_by_id(joke_id)

        is_session_user_author = (
            self.session_user_id != ANONYMOUS_USER and self.session_user_id == joke.user_id
        )

        author_joke_msgs = self.db_client.get_joke_msgs_by_joke_id(
            msg_type=JokeMsgType.AUTHOR.value, joke_id=joke_id
        )
        others_joke_msgs = self.db_client.get_joke_msgs_by_joke_id(
            msg_type=JokeMsgType.OTHERS.value, joke_id=joke_id
        )

        col_author, col_others = st.columns(2)

        with col_author:
            # Show selected joke
            self.show_joke(joke=joke)

            # Show author's messages
            st.subheader("作者想說")
            for joke_msg in author_joke_msgs:
                with st.expander(f"{joke_msg.user_id} ({joke_msg.timestamp})", expanded=True):
                    st.text(joke_msg.msg_text)

            # Author's message form
            with st.form(key="author_msg_form"):
                if is_session_user_author:
                    st.text_area("leave your message", key="author_msg_text")
                    is_author_submit = st.form_submit_button(label="Submit")
                else:
                    st.text_area(
                        "leave your message",
                        value="You're not the author of this joke!",
                        disabled=True,
                    )
                    st.form_submit_button(label="Not Author!")

        with col_others:
            # Show messages
            st.subheader("訪客留言")

            # message page_id selector
            n_pages = math.ceil(len(others_joke_msgs) / MSG_PER_PAGE)
            page_id = int(st.number_input("message page:", min_value=1, max_value=max(1, n_pages)))

            # show messages with selected page_id
            start_idx, end_idx = (page_id - 1) * MSG_PER_PAGE, page_id * MSG_PER_PAGE
            for joke_msg in others_joke_msgs[start_idx:end_idx]:
                with st.expander(f"{joke_msg.user_id} ({joke_msg.timestamp})", expanded=True):
                    st.text(joke_msg.msg_text)

            # Message form
            with st.form(key="others_msg_form"):
                if self.session_user_id != ANONYMOUS_USER:
                    st.text_area("leave your message", key="others_msg_text")
                    is_others_submit = st.form_submit_button(label="Submit")
                else:
                    st.text_area(
                        "leave your message",
                        value="嗨, 陌生人, 快去 user_profile 洗刷刷, 登入才能留言喔!!",
                        disabled=True,
                    )
                    st.form_submit_button(label="陌生人4ni")

        if is_session_user_author and is_author_submit:
            if st.session_state["author_msg_text"]:
                self.db_client.insert_joke_msg(
                    msg_type=JokeMsgType.AUTHOR.value,
                    msg_text=st.session_state["author_msg_text"],
                    joke_id=joke_id,
                    user_id=self.session_user_id,
                )
                st.experimental_rerun()
            else:
                raise Exception("please provide message content")

        if self.session_user_id != ANONYMOUS_USER and is_others_submit:
            if st.session_state["others_msg_text"]:
                self.db_client.insert_joke_msg(
                    msg_type=JokeMsgType.OTHERS.value,
                    msg_text=st.session_state["others_msg_text"],
                    joke_id=joke_id,
                    user_id=self.session_user_id,
                )
                st.experimental_rerun()
            else:
                raise Exception("please provide message content")


st.set_page_config(layout="wide")
joke_message_board_page = JokeMessageBoardPage()
joke_message_board_page.get_page()
