import arrow
import streamlit as st

# apps
from config import SupportPeriodTime
from module.issue_selector import IssueSelector
from tools.sqlite_client import SqliteClient
from tools.tool import display_image_or_video


class JokeDashbardPage:
    def __init__(self) -> None:
        self.issue_selector = IssueSelector()
        self.db_client = SqliteClient()

    def subtitle_template(self, period_time, day_issue, date_start):
        if period_time is SupportPeriodTime.DAY:
            return f"天下就可大會 {day_issue} 期 ({date_start})-日投稿"

        if period_time is SupportPeriodTime.WEEK:
            return f"天下就可大會 {day_issue} 期 ({date_start})-週投稿"

        if period_time is SupportPeriodTime.MONTH:
            return f"天下就可大會 {day_issue} 期 ({date_start})-月投稿"

    def get_page(self):
        st.title("天下第一就可大會 成績單")

        period = st.sidebar.radio(
            "craft period:",
            (
                SupportPeriodTime.DAY.name,
                SupportPeriodTime.WEEK.name,
                SupportPeriodTime.MONTH.name,
            ),
            index=1,
        )

        period_shift = int(st.sidebar.number_input("last-period", value=0))

        issue, date_start, date_end = self.issue_selector.get_issue(
            SupportPeriodTime[period], period_shift=period_shift
        )

        self.subtitle_template(period_time=period, day_issue=issue, date_start=date_start)

        date_start = arrow.get(date_start).datetime
        date_end = arrow.get(date_end).datetime

        date_start = str(st.sidebar.date_input("craft period starts from:", date_start))
        date_end = str(st.sidebar.date_input("craft period ends at:", date_end))

        jokes = self.db_client.get_jokes(date_start, date_end)

        n_per_row = int(st.sidebar.number_input("n_per_row", min_value=2))

        # sort via score
        jokes = sorted(jokes, key=lambda x: -x.score)

        authors = sorted([i.nickname for i in jokes])

        keyword_search = st.sidebar.text_input("keyword search")
        selected_authors = st.sidebar.multiselect("author filter:", sorted(set(authors)), None)

        if len(jokes) == 0:
            st.subheader("這段期間沒有投稿唷 ^.,>")
        else:
            queue = st.columns(n_per_row)[::-1]

            for idx, joke in enumerate(jokes, 1):
                if not queue:
                    queue.extend(st.columns(n_per_row)[::-1])

                # joke_id, joke_title, joke_content, joke_image_url, date, score, author = joke
                if selected_authors:
                    selected_authors = set(selected_authors)
                    if joke.nickname not in selected_authors:
                        continue

                if keyword_search:
                    if not (
                        (keyword_search in joke.joke_title)
                        or (keyword_search in joke.joke_content)
                    ):
                        continue

                col = queue.pop()
                with col:
                    st.subheader(f"rank: {idx}, score: {joke.score}")
                    st.subheader(joke.joke_title)
                    st.text(f"commit_date: {joke.date}, author: {joke.nickname}")
                    with st.expander(f"[{joke.joke_id}] 你可能會很意外的 point"):
                        st.text(joke.joke_content)
                        if joke.joke_image_url:
                            display_image_or_video(joke.joke_image_url)


st.set_page_config(layout="wide")
joke_dashboard_page = JokeDashbardPage()
joke_dashboard_page.get_page()
