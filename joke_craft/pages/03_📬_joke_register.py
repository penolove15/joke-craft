import streamlit as st

# apps
from config import ANONYMOUS_USER
from tools.sqlite_client import SqliteClient
from tools.user_info_manager import UserInfoManager


class JokeRegisterPage:
    def __init__(self) -> None:
        self.user_info = UserInfoManager()
        self.session_user_id_ = self.user_info.get_user_id()

    @property
    def session_user_id(self):
        if self.session_user_id_ is not None:
            return self.session_user_id_
        else:
            return ANONYMOUS_USER

    def get_page(self):
        st.title("天下第一就可大會 參賽報名表")
        if self.session_user_id == ANONYMOUS_USER:
            st.write("嗨, 陌生人, 不想當陌生人? 快去 user_profile 洗刷刷, 登入才會有成就!!")
        else:
            st.write(f"嗨, {self.session_user_id}, 趕快投！！！")

        db_client = SqliteClient()

        with st.form(key="joke information"):
            nickname = st.text_input("Who are you?")
            title = st.text_input("title")
            content = st.text_area("content")
            image_url = st.text_input("image_url / youtube_url")
            is_submit = st.form_submit_button(label="Submit")

        # form not clicked
        if not is_submit:
            return

        if content and nickname:
            db_client.insert_jokes(
                title, content, image_url, nickname, user_id=self.session_user_id
            )
        else:
            raise Exception("please provide at least content and author")

        st.text("your joke registered")


st.set_page_config(layout="wide")
joke_register_page = JokeRegisterPage()
joke_register_page.get_page()
