import streamlit as st
from module.tabs.tab import ProfilePageWithTabs


st.set_page_config(layout="wide")
user_profile_page = ProfilePageWithTabs()
user_profile_page.get_page_with_tabs()
