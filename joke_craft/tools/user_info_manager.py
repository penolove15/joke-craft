import datetime

import extra_streamlit_components as stx
import streamlit as st

# apps
from config import USER_INFO

JOKE_CRAFT_USER_INFO_COOKIE = "joke_craft_user_info"


def get_cookie_manager():
    cm = stx.CookieManager()
    cm.get_all()
    return cm


class UserInfoManager:
    def __init__(self):
        self.cookie_manager = get_cookie_manager()

    def get_user_info(self):
        # session first
        if st.session_state.get(USER_INFO) is not None:
            return st.session_state[USER_INFO]

        user_info = self.cookie_manager.get(JOKE_CRAFT_USER_INFO_COOKIE)
        if user_info is not None:
            st.session_state[USER_INFO] = user_info
            return user_info

    def get_user_id(self):
        user_info = self.get_user_info()
        if user_info is not None:
            return user_info["email"]

    def set_session_user_info(self, user_info):
        st.session_state[USER_INFO] = user_info

    def set_cookie_user_info_from_session(self):
        if st.session_state[USER_INFO] is not None:
            self.cookie_manager.set(
                JOKE_CRAFT_USER_INFO_COOKIE,
                st.session_state[USER_INFO],
                expires_at=datetime.datetime(year=2050, month=1, day=1),
            )

    def delete_user_info(self):
        self.cookie_manager.delete(JOKE_CRAFT_USER_INFO_COOKIE)
        # since del might not work properly, set it as None
        # as discuss within: https://discuss.streamlit.io/t/session-state-del-a-value/14785
        st.session_state[USER_INFO] = None
        self.cookie_user_info = None
