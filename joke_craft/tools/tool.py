import logging
import os
from urllib.parse import urlparse


import arrow
import streamlit as st


def set_logger():
    logger = logging.getLogger()

    if logger.handlers:
        logger.handlers = []

    console_log = logging.StreamHandler()
    console_log.setFormatter(logging.Formatter("[%(asctime)s %(levelname)-8s] %(message)s"))
    console_log.setLevel(logging.INFO)

    file_log = logging.FileHandler("./joke-craft/logs/log_file.log")
    file_log.setFormatter(logging.Formatter("[%(asctime)s %(levelname)-8s] %(message)s"))
    file_log.setLevel(logging.DEBUG)

    logger.addHandler(console_log)
    logger.addHandler(file_log)


def get_now():
    timezone = os.environ.get("TIMEZONE", "GMT")
    now = arrow.now(timezone)
    return now


def display_image_or_video(url):
    try:
        if "youtube" in urlparse(url).netloc:
            st.video(url)
        else:
            st.image(url)
    except Exception:
        st.text("J個圖片網址怪怪的, 請靜待修改上線, 或者聯絡on-duty")
