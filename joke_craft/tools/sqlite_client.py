import logging
import json
import os
import sqlite3
from dataclasses import dataclass

import arrow
import streamlit as st


@dataclass
class JokeModel:
    joke_id: int
    joke_title: str
    joke_content: str
    joke_image_url: str
    date: str
    score: float
    nickname: str
    user_id: str


@dataclass
class JokeVotesModel:
    user_id: str
    date: str
    joke_id: int
    score_change: float


@dataclass
class UserBadgeModel:
    user_id: str
    type: str
    date: str


@dataclass
class JokeMsgModel:
    msg_id: int
    msg_type: str
    msg_text: str
    joke_id: int
    user_id: str
    timestamp: str


@dataclass
class WarriorImageModel:
    image_id: int
    name: str
    part_type: str
    data_uri: int


class SqliteClient:
    def __init__(self, db_path=os.environ.get("DB_PATH"), check_same_thread=False) -> None:
        self.connection = sqlite3.connect(db_path, check_same_thread=check_same_thread)

    def __del__(self):
        self.connection.close()

    def get_cursor(self):
        return self.connection.cursor()

    def get_jokes(self, start_date, date_end=None):
        if not date_end:
            sql = f"""
                SELECT * from Jokes WHERE date >= '{start_date}'
            """
        else:
            sql = f"""
                SELECT * from Jokes WHERE date >= '{start_date}' and date < '{date_end}'
            """
        logging.info(sql)
        cursor = self.get_cursor()

        cursor.execute(sql)
        jokes = cursor.fetchall()
        self.connection.commit()

        return [JokeModel(*joke) for joke in jokes]

    def insert_jokes(self, title, content, image_url, nickname, user_id):
        timezone = os.environ.get("TIMEZONE", "GMT")
        hour_shift = int(os.environ.get("REGIST_HOUR_SHIFT", 0))
        now = arrow.now(timezone).shift(hours=hour_shift)
        date = now.format("YYYY-MM-DD")
        score = 100

        cursor = self.get_cursor()
        cursor.execute(
            """
            INSERT INTO Jokes (title, content, image_url, date, score, nickname, user_id) VALUES
            (?, ?, ?, ?, ?, ?, ?)
        """,
            (title, content, image_url, date, score, nickname, user_id),  # noqa
        )
        self.connection.commit()

    def insert_joke_votes(self, user_id, joke_id, score_change):
        timezone = os.environ.get("TIMEZONE", "GMT")
        hour_shift = int(os.environ.get("REGIST_HOUR_SHIFT", 0))
        now = arrow.now(timezone).shift(hours=hour_shift)
        date = now.format("YYYY-MM-DD")

        cursor = self.get_cursor()
        cursor.execute(
            """
            INSERT INTO JokeVotes (user_id, date, joke_id, score_change) VALUES
            (?, ?, ?, ?)
        """,
            (user_id, date, joke_id, score_change),  # noqa
        )
        self.connection.commit()

    def get_votes_by_user_id(self, user_id):
        sql = f"""
            SELECT * from JokeVotes WHERE user_id = '{user_id}'
        """
        logging.info(sql)
        cursor = self.get_cursor()
        cursor.execute(sql)
        joke_votes = cursor.fetchall()
        return [JokeVotesModel(*joke_vote) for joke_vote in joke_votes]

    def get_joke_by_id(self, joke_id):
        sql = f"""
            SELECT * from Jokes WHERE joke_id = {joke_id}
        """
        logging.info(sql)
        cursor = self.get_cursor()
        cursor.execute(sql)
        joke = cursor.fetchall()[0]
        return JokeModel(*joke)

    def get_joke_by_user_id(self, user_id):
        sql = f"""
            SELECT * from Jokes WHERE user_id = '{user_id}'
        """
        logging.info(sql)
        cursor = self.get_cursor()
        cursor.execute(sql)
        jokes = cursor.fetchall()
        return [JokeModel(*joke) for joke in jokes]

    def update_joke_id_score(self, joke_id, score, cur):
        if score > 0:
            sql = f"UPDATE Jokes SET score = score + {score} WHERE joke_id = {joke_id}"  # noqa
            cur.execute(sql)
        elif score < 0:
            sql = f"UPDATE Jokes SET score = score {score} WHERE joke_id = {joke_id}"  # noqa
            cur.execute(sql)
        else:
            print(score, "not updated")

    def winner_joke_update(self, candidates, winner, vote_user_id):
        cursor = self.get_cursor()

        for candidate in candidates:
            if candidate != winner:
                self.update_joke_id_score(candidate, -1, cur=cursor)
                self.insert_joke_votes(vote_user_id, candidate, -1)
            else:
                self.update_joke_id_score(candidate, 1, cur=cursor)
                self.insert_joke_votes(vote_user_id, candidate, 1)
        self.connection.commit()
        st.write(f"updated winner: {winner} among {candidates} by: {vote_user_id}")

    def insert_user_badge(self, user_id, badge_type, date):
        cursor = self.get_cursor()
        cursor.execute(
            """
            INSERT INTO UserBadges (user_id, type, date) VALUES
            (?, ?, ?)
        """,
            (user_id, badge_type, date),  # noqa
        )
        self.connection.commit()

    def get_user_badges_by_user_id(self, user_id):
        sql = f"""
            SELECT * from UserBadges WHERE user_id = '{user_id}'
        """
        logging.info(sql)
        cursor = self.get_cursor()
        cursor.execute(sql)
        return [UserBadgeModel(*badge) for badge in cursor.fetchall()]

    def get_joke_msgs_by_joke_id(self, msg_type: str, joke_id: int):
        sql = f"""
            SELECT * FROM JokeMsgs WHERE msg_type = '{msg_type}' and joke_id = {joke_id}
        """
        logging.info(sql)
        cursor = self.get_cursor()
        cursor.execute(sql)
        joke_msgs = cursor.fetchall()
        return [JokeMsgModel(*joke_msg) for joke_msg in joke_msgs]

    def insert_joke_msg(self, msg_type: str, msg_text: str, joke_id: int, user_id: str):
        timezone = os.environ.get("TIMEZONE", "GMT")
        timestamp = arrow.now(timezone).format("YYYY-MM-DD HH:mm:ss")

        cursor = self.get_cursor()
        cursor.execute(
            """
            INSERT INTO JokeMsgs (msg_type, msg_text, joke_id, user_id, timestamp)
            VALUES (?, ?, ?, ?, ?)
        """,
            (msg_type, msg_text, joke_id, user_id, timestamp),
        )
        self.connection.commit()


class WarriorImagesSqliteClient(SqliteClient):
    def insert_image_example(self, name, part, data_uri):
        cursor = self.get_cursor()

        name = "test"
        part = "left_foot"
        data_uri = "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAoHCBUWFRgWFRUSFRUYFRgYEREREhESERESGBgZGRgVGBgcIS4lHB4rHxgYJjgmKy8xNTU1GiQ7QDs0Py40NTEBDAwMEA8QHxISHjQhISE0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDE0NDQ0NDQ0NDQ0NDQ0NDExNP/AABEIALsBDgMBIgACEQEDEQH/xAAbAAABBQEBAAAAAAAAAAAAAAAFAQIDBAYHAP/EAD0QAAIBAgQDBQYFAgUEAwAAAAECAAMRBAUSIQYxQRMiUWFxMlKBkaLRFBVCobEHIzNiksHwU3KC8RYkY//EABkBAAMBAQEAAAAAAAAAAAAAAAECAwQABf/EACQRAAICAgEEAwEBAQAAAAAAAAABAhEDITEEEhNRIjJBYXEU/9oADAMBAAIRAxEAPwDo4MUGRiPEkMOiOdosjrHun0M4JhsY96r/APcYb4ew4NO/iZnqjd9/Nj/M0uRAqi+cj0/3bNefWNIvBCJMt4p35yRQLTd3aMLhsp1EJceXOWkG8RhveJRIMZeyT5Jisgx+NSihdzYCV82zVKCFnYCw2HiZyTiji2piRo2VAdrcz4XgchlE0mf/ANQgQVoA+bHpOd4jGO5JZiSTckkysWkTOYl2OlRZ7e08tYmVrxytbeAIUweZ1U2V3UHmASBD2H4traAjsXAbm1ybXmPFQ+ElSpaBq1QYunaOrZdjVdNSkb8/I+E3uR/4SzgeT5k1NxY90kah0ncMkrf2k8xeZseHxydcM05c3kgr5QcJiapCtS8dqmizKSap7VI9U8GnWcS3iyK8UGCziZY4yNDH3hRwypymbxftn1mjqHaZ3Ee0fWBhRBKOMEv2gfNcVoI9YoyPKkkCCQYesrdZbgoN0aIRwMjjgYRB95Fim7h9I+8rY9rIx8pz4CuTDsd2Pmf5mwypRoWx6TGk7E+Jmvype6PSR6ZX3M19TKlFBIiNdrDaeERyD1mtGOWltkaVLz2sKCfjFalyIgDjPFtTwzshs1rA+sq2q0QSZznjXODWrsL91CVt0uJmC0dWckkk3JNyT1JkTGTKoWNJiAGW8NlzvyBt4wNpDJN8FQDePKGaDC8O7gsT8oZTLUFu7y6yUs0VwWj00pc6MWmFc8lPyMtYfKKr8kPx2my7IDkBJqL6Yvnv8KPpa/THV8prUxqZLL43vOo8A5v2mGCE9+n3WvzK/pb/AJ4QSzq6lSAQRYgwfw+/4bFgckfuHz1eyfnaMpWSlDtOqUKsuAwTRfeEqL7QokyWeiT0IB147VGRYAk1IyWQUTJoyAR1TtAFbmfWH6vKAH5n1gYURWmZ4j6es05mY4hO4k5ukVx7YJw1VlOx+EM4fHAjeCKax2iSjPRSUDo0UTLLxb/+R/1j7R44rH/Sb/WJTyw9ieCfo0+qUc3e1NvQ/wASlhM/V3CFGUnkbgyXPKn9o+k5yTi2gRi4zSZk05D1H8zWYZ7BbX5C8y1Lmo8WE1asAo9IekWmU6t7RaepcRKfKVgx8NpNSflNlUYbbLtM7TE/1Mwh/DawT3WBO+1r77TbEzLcfH/6dQeX+8jbss4pI4oTLWGwDvyHxMip0rma7LqYVAB4RZy7UUw4+57KmByNRu25huhhgosBGqZZp3OwmSUnLk3wxxjwh6UZKKIklOh5yZUiUVsH1KdpWcGGzhyZA+EE5OgSSYOpMYuJwoexBsy+yfMbyPEYpEO5+EXD5hTbkwB8DLxvkxzrizb5Ziu0po/Ikd4eDDZv3BhnDPMtkNUBXBIAuGFza19j/A+cP4WqDyII8QbyqdmWSoK6p68ajXixhB94t4wRSIA2S0jJwZXpSVTGQBuIPdMBNzMOYi+kwG0DChhmW4gPeE1Uyme+2JHL9SuL7FGkI5mi0htPMszxZokiQ5ZU/wCmfmJCmFZX7wtbpN0yTLY8/wBwzsmNRVobDmlJ0xmAH94eUM8QvanbxgfKBesYQ4nY6VHmJSOsYkt5UBqAu6DzmuRRtMnhFvVSaxJbptRJdU7kJVW4kSJJqh2nsPNNkET4a9jcnylPiDCdph3T3lI/aEqaxXS4I8oj5C5ao4Fh6JBIPMGx9RD2DFhG8Q4PssS6gWDHUo9ef73ktBbCRymjATrL+DQ3vB7VAouekqDM8Qf8OkSOjWJkYxcjVLIomw7oki6W5GYunmGKJsw0jzAhTDYxup9TOlGgxyd35QdxFYhbCZ7NMfU9lNievWXMSFZbrcnqSTzgHtDrsQQOtjpa3gD0ggrZ2XUSJMtF9eJqD/sL7n1kivhgbIVv0sSY7EZbQLh1GkjkgJcX8Tq5mWsNgQ24QDxYjcysmvZnjB/iLdKprQoDv0PmOUucPZgabqpPdY6WU9D0YSklPSw2kbqNdxzVwfUX3iqQ/ZymdPo1peUwJQqbD0EL0DtL2YKBXEuTVMUqImIq4dQ5NY0dnqJawUNcW3/9TlXFOBfLMVQXBYvEVKr7vSdwzhiwCKwGzBrnYjpO112YIxUXYKxQc7tY2HzmA/prw3Up9ricZScYtqh0PWsXClQSy+BJZgT5WjRAVs64H04apiMVmOJNVUZ+0ZwtEPYlUCHfc2AAI8hKPA2V4zMsORXxuKTD02ZUVGOusSASC55qvnf2rbWhjjnI8RjMbhafZ1WwS6WrshsgZqjCoTY89AUX6ajbnNjnOHang6qYRdDrQdcMlIBdL6SF0Dod7wnHKOyqYDNqWFweJrV0ZkFZHbXYMT2iOo2JVBqvYEX8p1VxM7/Tnhv8NhjUrUimLqM/atV3qhNVlUHoCBc+JO/SaNxFkFEZmSzs9+a4iY/OW78jm4LYeSGhFqCJhzDVGmCNwDM8I2zRN0rD1Q7GZLFt32mqrt3T6THYhu+0fNwifTLbZb4f3qMZPxK26jzjOGBux843iF++ohesYVvKQZcl6w8hNSomZygXqnyWaGoTayjczRgXwRLPuY6o1+Xxj6KbXktKmAtj8Y9l2sBKtkuNDqLSSV15SXX3SfATjmjD/wBQMu3SqByNmt0B6/P+YAoDaEs2zGtiHdNRC6iAgAsANrecGBCvdPMTPkknwacUJR2/0cKIY3bdR06RlfPT7FGmXtsW3Cj023jlTVseXhLgwrAdzb0sJJNLkv2trQLoamVnriprJ7tNW0Ig8T1kuGptbkbdLm8vU8Fvd7sfDpJcSNIudj0UTpTtUgwxdvLJaFraesFYynZt/HnL2CJLS1jcJcd4WuNpNOmXkriUsEit1X9rwstCwgLslQbHvX28TL+Gx21j840kJF/hLiU2gTGkq4PTTv6wrXxAlalQNZgAL72J6AeMME6Em0nZt8E90Q+KKf2EN4VtoGoppCgcgAPkLQvheU0o818jsfSL03RTpZ0dFbcaWZSAbjfmZnuCcgxGDp1ExFYVmdwysGd9KhbW73nNODEc7RgGUzzhjE18bQxFPECnRpml2lHVVBfs6hdu6vdN1IG80XEmAevhqtKk4pu62RyWUIdQN7ruOXSXqZ2j4wDPcK5RVwuFFGvVFZxUdi4Z2BVrWF23l5hL1blKpEUJAy7GYrNz/cM27jY+kw+a/wCIZHNwWw8jKAhfDPtBNMWl6jWUDcyGPktk4NFiTZD6TE4tiST5zZY97IZjK3ImNm5R3T8MOcML3CfMyrnjXqrL/DQ7l4LzRr1v+ecaWoIEN5GW8kXvsYeUm4gTIBux84Xq1CCLTTiXxRHLubLoEk1+EqI5lmgYaEaPFdp5VuhHiJMu8UiEDZzrFYYpVfpcki3nB1XCsBqPMnl4TacS4Q6dajcbn0mU/FKVNwSegmScWpHo4pKWP/Ctg13h7DIGmfovbyl/D4q0SXI8XoJ1wqAnmekGdiT323v08o6tiNW0SrjABvOSGsifH6T3Et5mNxmZOydb25k7CQa2c90BV6s3M+gitgV5l2by2AjqKElJvgEUqo1XZrk/Ey4MSBzl2hlrP7CbeIG3zlHOsC9Ngpt0JA8I/bZJtpaZYU3+Img4bWxceSmAF5D0mi4cXd28lH8mLFbFy/VsPrCmF5QUkK4fkJZGMsRjco6MqRhSwh2jyYxOUUmccMqcpXMncyFjCjiKqNj6TC5if7jes3Vc90+k5/iH1VXHnI51aVF8HLJF5SOuxsI4bSYKCJDHyWyPRoc2ayH0mTqDuTT543c+EzOJ9idkdyGwfQ0mQi1IekB45h2zXIHqbQ/lYtTHpOc4rhZaj1HrV6rNqJ16hZVJJAOoHYD0lZRi0k3RKDkpNpWbrID3WPiTyhCvUFxuL+Fxf5TkHB2Er4io+Gp4ipToWLVGTVYqpsAu/dLX+Nutpe4z4ToYKklWlWq9oXAC1GTUwsSWXSARbx35zTFdqSM8pXKzrCVRb/glnD1RzBB8wbzmOT8MvjqFOti6+I0lAEpIQNlJAc6rglgAeW/O8DYai2DzRKOEq1HXtaautwbq1taOF7rWBJvbb1ELR3cdwDySQUpJOoVsbiKSupB5TJY/h8qxZNwf0/aa4xjITFlBS5GjklHg51i8rqIpcoVUdTzPmBKNp0HN6LuujyPy6zCFbEqeYJBkMkO2jVgy912V3cgXErYIvUqBOp6n/aXrDlFFDkRsRyI5xYtLkrJN8MPUeGHsCXufAbCGcDw/SUEuAzdL8hMvQzWsg0hzysL8x6S1SxdVubH4k29ZTuj6FUJtU2aTG5nQoqFWxIFlRLbevhMRmtY1XLt16dAPCWsS6i9yCYNeqDA5DeOMV/RjHaarhtR2RPUsb/DYTKMbAmHuEa+pHHg1wPIidFbI5W6NPSG8K0eUE0jvCiHaOZWSmNcxC0iduXrCcXlO0QmNVtohaEAjmRMY52kTNCjhmJayN6Tn2FN6zn/MZvMa/cb0mEwBGtj4uYk/wpjZNiVIbyMkpnaT4qjqXbmJWG0l207LN2gvxC/dgDEnZR5iGeIW5DzgWuO8g8xIz+5bFqBq8MLUv/Gcr41xFQnQurSzMW0Am4WwANum5nVDtS/8ZzLiKpiVYdhr0nVr0hSL32vf4zRfyRnXDNzwlgKdDDqFULfd3PtO3VmMxGXYQ43MKz4gM6IzdmjBtBVX0ogvtpA3t168zC2ArVnQambSANhsDIuD8RixiHGI7TsAj6NSqF16102IF/Z1TUlpMzy5DfF+Jqpg6hpag5CougHUqswU6bcjpvy5SL+nOQpSoJXKHt6gYu7g6lXUQFUHlyBPj8pa4ox1b8OfwZbttaW0BWbRvq2YWhHhDE1mwyfiSe3u/aawqt7babhdvZ0zm9gRoqY2kyrI6ZEkvaKcIwiqRKlfEb2irUtuYyiB6JtN2J/y2nPeJMLorE9G3+M3yYtb84B4wwoZA4tcRMkPjsphlUjFCpLFOpKDi0cjzL2m5SL7DwjXL9CfnGpVlmkR1g2h07BvYux7xJ8pIUtyhKpYQdjKyjqBOts50lbKWY17LpHNv2Es5JmHZOD0Is33gI1tblunJfSTsZWq0ZW+7Z07DYtXAKkGE6GK2sZzrhipUeqqLcgnfwA6mdMfLhp22MeMWyMlR7txEL3Ig2ujobbxKGK3F5wtB4NELSFawMczbQgPM0jLRrtI9UNnUR5k9kPpOdU8Rpffa5O83ua1P7Z9DOcY7ax/zRJbZSOlZoqOMsdLb35GPqL4QWzrZSeW1iOkKUd1HWL+DosZ813UecGuL1EHnL2cG9UfGVKAvXUeUzvczQtYzS4k2pn0mHxjHSbdTNlmr2pH0mDxGKB2+cee5CYvqzRZRT7gHSwh+lhUI5TEYfOGUAACw2EvUs/YdJeObVMSWKzXLlqeEVcuUcrj0mdpcTW5gy2nEy9QY3lj7EeGXo0+GTSLXv5yc8pmV4mTrf5SwnEtLxneSL/RfFL0Ffw3UxDSMorn9I/qEnTOKR/UvzjxyL2K8cvRSxuEYtddvnKWPp1ShQ7j94cGOpk+0PmJIayEcx8xK+SMlTJ+Np2c6q4boRYiUK1MqeV5p89pqKnd3Ft7coNrUwZ57fbKj0opSjYEq1GttKiZ0ymxHKGnw0CY3KjckSkZRfJKUZR+pO+eg+MF4nFPUNgDaXcJk5JGqF6eXqOQELlGPAFGclsC4LDFRvzlvsiekIvhrQxw7kZruLg6B7R8fKIpOTpFO1QjsN/0/wAm0IazjvN7F+izYXE8iBFCrsALCMczZGPaqMMpd0rIq2HDdINqZaL3hTVPFgYJRTOTBDoyxO2MLPSBlOphJNxYbKwqXEaXnnpkSEtFCQ5m/cMwGcCy/GbbM27syecYYlDbeJfyKKPxH4Qa6IPlCGGJCgQTw7XsCjD5w8tPa43E56OR7MGvW9BI8vW9f4T1dr1W9JJk4vVYzPH7GqWoBzMcKaiaBtfnM0/CZvsT85sljgJocUzKpNcGIPCr+JiHht/GboLHBYHBMZZZI563D1WeGTVR0nRlSO0DwEHYhvNI5q2V1R+mN/L6vumdL7FfAT34VfAQeJB879HNRhHH6Gi9k4/S3ynSfwae6Ihy5D+mDw/07/o/hzS7Do37whlGDes4A1BR7Tb7CbZsqpn9MtYXCKgsotvv5x4YW3tgn1CrS2ZfinDLSpIF5l9yedgDAlMahNhxVgjUpi3NTcesxmEaxKnY+E7PGnofpp2qFanFXCyYHeT1CAvnM9mlpFJacmWnJsNhXf2VJ8+k0uVcP8mf/T0lY45SZKeWMUBMtyN6rAnZOp8R5Tb4PCpSQIgAAEkVVQWAEYzXm2GNQX9MGTK5v+Cs8iLR95Exjkz154GNMVRFYULeLeJPQBPMgPOUq+DlsmODX2MVqzjN5phW07b2gRqdx/M21anbbpAePwFrso2/Uv8AvJSiy0JVoztLAqDe0mNK3Iy0FttIni3ZSTRADd3PnLeQjvsfOD6LXLnzMJcPL7R85nh9iuTUTRryjhGJHiaDISLHCNWOE44escsQRyzgDxFiAT1462Bj7z14y0eqx4xBYqiOnokqKPZAwsesDV+HUZtVhfxhtWj0ac4p8hUnHgz3/wAWUkHf5mXqGQIOYB9YY1xrVIFjj6GeWT/RlLCog2Aiu9o0vGtHomNO89F5RsJwjSNhHmMEVnI8qzxjzGQUESIzRTIjFo4cYxjHAxl7mdQScjUPOUnW3OWabd6SVaYIgaOM1mGDsdSjY8x4GCKvOaqp1BmdzgKjDpeSlGtlFK1QJw7bMfOGuHl7l5lKNZtJ3/YQ3kmKfTz/AGX7TNBU7NWZ3E1qSRYDXG1Pe+lftJFxtT3vpX7S9GUNrHCBFxtT3vpX7RwxtT3vpX7TqBYdEVYFXG1Pe+lftHDG1Pe+lftCkCw2N44CBVxtT3vpX7Rwx1T3vpX7SySQoaUSQCA/zCp730r9oozCpb2vpX7RloAbiQJ+YVPe+lftPfmFT3vpX7Q2AORQYC/MKnvfSv2i/mFT3vpX7TrODpM9AX5hU976V+0UZhU976V+0JweAiCA2zCp730r9p78wqe99K/aGzg20Y0C/mFT3vpX7RpzCp730r9oDg5GLA5zCp730r9oxcwqe99K/aCzg6RGsIG/MKnvfSv2jTmFT3vpX7TrCGGjSIHONqe99K/aI2Nqe99K/aCwhZjYRUTxgZ8bUuO99K/aOfG1Pe+lftAcGJ5anSA/xtS/tfSv2jPxtT3vpX7Tji7muJVAXYgADcmcn4j4mas9qeyqTY+MLf1Ex9Q01BY2LbgAC+3lMCIrCf/Z"  # noqa E501

        cursor.execute(
            """
            INSERT INTO WarriorImages (name, part_type, data_uri) VALUES
            (?, ?, ?)
        """,
            (name, part, data_uri),  # noqa
        )
        self.connection.commit()

    def insert_image(self, name, part, data_uri):
        cursor = self.get_cursor()

        cursor.execute(
            """
            INSERT INTO WarriorImages (name, part_type, data_uri) VALUES
            (?, ?, ?)
        """,
            (name, part, data_uri),  # noqa
        )
        self.connection.commit()

    def get_image_by_id(self, image_id):

        sql = f"""
            SELECT * FROM WarriorImages WHERE image_id = '{image_id}'
        """
        logging.info(sql)

        cursor = self.get_cursor()
        cursor.execute(sql)
        images = cursor.fetchall()
        return [WarriorImageModel(*image) for image in images]

    def get_images_by_part_type(self, part_type):

        sql = f"""
            SELECT * FROM WarriorImages WHERE part_type = '{part_type}'
        """
        logging.info(sql)

        cursor = self.get_cursor()
        cursor.execute(sql)
        images = cursor.fetchall()
        return [WarriorImageModel(*image) for image in images]


class UserWarriorImagesSqliteClient(SqliteClient):
    def insert_user_image_info(self, user_id, image_info_json):
        cursor = self.get_cursor()

        cursor.execute(
            """
            INSERT INTO UserWarriorImages (user_id, image_info_json) VALUES
            (?, ?)
        """,
            (user_id, image_info_json),  # noqa
        )
        self.connection.commit()

    def get_user_image_info(self, user_id):
        sql = f"""
            SELECT * FROM UserWarriorImages WHERE user_id = '{user_id}'
        """
        logging.info(sql)

        cursor = self.get_cursor()
        cursor.execute(sql)
        result = cursor.fetchall()
        if result:
            user_id, image_info_json_str = result[0][0], json.loads(result[0][1])
        else:
            user_id, image_info_json_str = None, None
        return image_info_json_str

    def update_user_image_info(self, user_id, image_info_json):
        sql = f"""
            UPDATE
                UserWarriorImages
            SET
                image_info_json = '{image_info_json}'
            WHERE user_id = '{user_id}'
        """
        logging.info(sql)

        cursor = self.get_cursor()
        cursor.execute(sql)
        self.connection.commit()
