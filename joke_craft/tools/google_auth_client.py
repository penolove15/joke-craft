import asyncio
import json
import os
import requests
import streamlit as st

from config import ANONYMOUS_USER
from httpx_oauth.clients.google import GoogleOAuth2
from tools.user_info_manager import UserInfoManager

GOOGLE_OAUTH_ACCESS_TOKEN = "google-oauth-access-token"


class GoogleAuthClient:
    def __init__(self) -> None:
        self.user_info_manager = UserInfoManager()
        self.session_user_id_ = self.user_info_manager.get_user_id()

        self.oauth_credential_path = os.environ.get("OAUTH_CREDENTIAL_PATH")
        self.redirect_uri = os.environ.get("OAUTH_REDIRECT_URL")

    def set_google_auth_client(self):
        with open(self.oauth_credential_path, "r") as read_file:
            credential = json.load(read_file)
            client_id = credential["web"]["client_id"]
            client_secret = credential["web"]["client_secret"]

        self.client = GoogleOAuth2(client_id, client_secret)

    async def get_login_page(self):
        authorization_url = await self.client.get_authorization_url(
            self.redirect_uri,
            scope=["email"],
            extras_params={"access_type": "offline"},
        )
        st.write(
            f"""
            <h1>
                <a target="_self" href="{authorization_url}">Login</a>
            </h1>
            """,
            unsafe_allow_html=True,
        )

    async def get_access_token_and_set_session(self):
        code = st.experimental_get_query_params()["code"]
        token = await self.client.get_access_token(code, self.redirect_uri)
        st.session_state[GOOGLE_OAUTH_ACCESS_TOKEN] = token
        return token

    @property
    def is_oath_env_varialbes_set_properly(self):
        return self.oauth_credential_path is not None and self.redirect_uri is not None

    @property
    def session_user_id(self):
        if self.session_user_id_ is not None:
            return self.session_user_id_
        else:
            return ANONYMOUS_USER

    @property
    def is_anonymous_user(self):
        if self.session_user_id == ANONYMOUS_USER:
            return True
        return False

    def get_user_info(self):
        def get_user_info_via_access_token(access_token):
            r = requests.get(
                "https://www.googleapis.com/oauth2/v3/userinfo",
                params={"access_token": access_token},  # noqa E501
            )
            return r.json()

        # session not get -> from cookie
        user_info = self.user_info_manager.get_user_info()

        # still empty from token
        if user_info is None:
            token = asyncio.run(self.get_access_token_and_set_session())
            user_info = get_user_info_via_access_token(token["access_token"])
            self.user_info_manager.set_session_user_info(user_info)

        return user_info

    def share_check_property(self, obj):
        share_property = {
            "is_anonymous_user": self.is_anonymous_user,
            "user_info_manager": self.user_info_manager,
        }
        for name, value in share_property.items():
            setattr(obj, name, value)
