### POETRY (PYTHON DEPENDENCY) ###

.PHONY: lock
lock:  ## Lock python dependencies
	poetry lock --no-update

.PHONY: install
install:
	poetry install --no-dev

.PHONY: install-dev
install-dev:
	poetry install

### CODING STYLE ###

.PHONY: black
black:  ## Format python files
	black -l 99 --include .py$$ .

.PHONY: lint
lint:  ## Execute linter
	flake8 --max-complexity 12

### DOCKER ###

IMAGE_NAME := joke-craft
BUILD_TAG := dev

.PHONY: docker-build
docker-build:
	docker build -t ${IMAGE_NAME}:${BUILD_TAG} .
