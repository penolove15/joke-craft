FROM python:3.8.9-slim-buster

ENV POETRY_HOME /srv/poetry
ENV WORKDIR /tmp
WORKDIR ${WORKDIR}

# install dependencies
ADD ./poetry.lock ${WORKDIR}/
ADD ./pyproject.toml ${WORKDIR}/

RUN apt update && apt install -y \
    ca-certificates \
    curl \
    gcc \
    git \
    libgomp1 \
    libkrb5-dev \
    make \
    ssh \
    vim \
    # install poetry
    && curl -sSL https://install.python-poetry.org | python3 - \
    && chmod +x $POETRY_HOME/bin/poetry \
    && ln -s $POETRY_HOME/bin/poetry /usr/local/bin/poetry \
    && poetry config virtualenvs.create false \
    && poetry install --no-dev

WORKDIR /
