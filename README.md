# joke-craft

## start service

make image

```bash
make docker-build
```

```bash
docker-compose up -d  # a service default using port 54088
```

in this code base there already have a example sqlite db file: example.db

if you wanna create your own db, please check:

- create_table.py (table creation)
- docker-compose.yml (env:DB_PATH)
